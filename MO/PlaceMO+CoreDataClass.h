//
//  PlaceMO+CoreDataClass.h
//  App7RememberAll
//
//  Created by Alexander on 28/03/2019.
//  Copyright © 2019 Alexander. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GroupMO;

NS_ASSUME_NONNULL_BEGIN

@interface PlaceMO : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PlaceMO+CoreDataProperties.h"
